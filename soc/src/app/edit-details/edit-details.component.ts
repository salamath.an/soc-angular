import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewpostService } from '../shared/services/newpost/newpost.service';
import { Newpost } from '../shared/model/newpost/newpost.model';
import { KeycloakService } from '../shared/soc-keycloak/services';
import { UserKeyCloakService } from '../shared/services/userKeyCloakService/userKeyCloakService.service';
import { Router, ActivatedRoute } from '@angular/router';
import { userDetails } from '../shared/model/personaldetails/personal-details.model';
declare var $: any;


@Component({
  selector: 'app-edit-details',
  templateUrl: './edit-details.component.html',
  styleUrls: ['./edit-details.component.scss']
})
export class EditDetailsComponent implements OnInit {

  createPersonalDetails: FormGroup;
  public loggedinUserName;
  statusMessage = '';
  loader: boolean;
  personalDetails: userDetails[];
  myDetails;
  id: number = undefined;
  private sub: any;

  constructor(private fb: FormBuilder, private newpostService: NewpostService, public keycloak: KeycloakService, private router: Router, private activeRoute: ActivatedRoute, public userService: UserKeyCloakService) {
    this.loggedinUserName = this.userService.getUsername();
  }


  ngOnInit() {
    this.createPersonalDetails = this.fb.group({
      tusername: [''],
      tpassword: [''],
      fusername: [''],
      fpassword: [''],
      iusername: [''],
      ipassword: [''],
      whatupsno: [''],
      emailid: [''],
      postedby: ['']
    });

    this.sub = this.activeRoute.params.subscribe(params => {
      this.id = +params['id']
    });

    this.getUser();
  }

  getUser() {
    if (this.id) {
      this.newpostService.getUser(this.id).subscribe(
        (user: userDetails) => {
          this.editUser(user[0]);
          this.myDetails = user[0];
          // console.log('test' + this.myDetails);
        },
        (err: any) => console.log(err)
      );
    }
  }

  editUser(user) {
    this.createPersonalDetails.patchValue({
      tusername: user.tusername,
      tpassword: '',
      fusername: user.fusername,
      fpassword: '',
      iusername: user.iusername,
      ipassword: '',
      whatupsno: user.whatupsno,
      emailid: user.emailid,
      postedby: user.postedby,
    })
  }

  updateUser() {
    this.mapFormValuesToPersonalDetailsModel();
    this.newpostService.updateUser(this.id, this.myDetails).subscribe(
      user => {
        $('.soc-alert').addClass('alert alert-success');
        setTimeout(() => {
          $('.soc-alert').addClass('soc-alert-hide');
        }, 5000);

        this.loader = false;
        this.statusMessage = 'Successfully Saved.!';

        this.router.navigate([
          '/personal-details'
        ]);
      },

      err => {
        $('.soc-alert').addClass('alert alert-danger');
        setTimeout(() => {
          $('.soc-alert').addClass('soc-alert-hide');
        }, 5000);
        this.statusMessage = 'Problem with the services.! please try after sometime.!';
      });
  }

  mapFormValuesToPersonalDetailsModel() {
    this.myDetails = new userDetails();
    this.myDetails.tusername = this.createPersonalDetails.value.tusername;
    this.myDetails.tpassword = this.createPersonalDetails.value.tpassword;
    this.myDetails.fusername = this.createPersonalDetails.value.fusername
    this.myDetails.fpassword = this.createPersonalDetails.value.fpassword;
    this.myDetails.iusername = this.createPersonalDetails.value.iusername;
    this.myDetails.ipassword = this.createPersonalDetails.value.ipassword;
    this.myDetails.whatupsno = this.createPersonalDetails.value.whatupsno;
    this.myDetails.emailid = this.createPersonalDetails.value.emailid;
    this.myDetails.postedby = this.createPersonalDetails.value.postedby;
  }

  loaderDisplay() {
    window.scroll(0, 0);
    this.loader = true;
  }

}
