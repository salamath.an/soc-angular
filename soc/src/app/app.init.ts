import { KeycloakService } from '../app/shared/soc-keycloak/services/keycloak.service';
import { environment } from '../environments/environment';

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: {
            url: environment.settings.keycloakUrl,
            realm: environment.settings.keycloakRealm,
            clientId: environment.settings.keycloakClientId
          },
          initOptions: {
            onLoad: 'login-required',
            checkLoginIframe: false
          },
          bearerIncludedUrlPatterns:
            environment.settings.keycloakBearerIncludedUrlPatterns
        });
        resolve();
      } catch (error) {}
    });
  };
}
