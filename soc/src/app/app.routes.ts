import { Routes } from '@angular/router';
import { RecentPostComponent } from './recent-post/recent-post.component';
import { NewPostComponent } from './new-post/new-post.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AboutCompanyComponent } from './about-company/about-company.component';
import { EditDetailsComponent } from './edit-details/edit-details.component';

export const ROUTES: Routes = [
  { path: '', component: RecentPostComponent },
  { path: 'recent-post', component: RecentPostComponent },
  { path: 'new-post', component: NewPostComponent },
  { path: 'personal-details', component: PersonalDetailsComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'about-company', component: AboutCompanyComponent },
  { path: 'edit-details/:id', component: EditDetailsComponent },
  { path: '**', redirectTo: '' }
];
