import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewpostService } from '../shared/services/newpost/newpost.service';
import { Newpost } from '../shared/model/newpost/newpost.model';
import { KeycloakService } from '../shared/soc-keycloak/services';
import { UserKeyCloakService } from '../shared/services/userKeyCloakService/userKeyCloakService.service';
import { Router, ActivatedRoute } from '@angular/router';
import { userDetails } from '../shared/model/personaldetails/personal-details.model';
declare var $: any;

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {

  createPersonalDetails: FormGroup;
  public loggedinUserName;
  statusMessage = '';
  loader: boolean;
  personalDetails: userDetails[];
  myDetails: userDetails[];
  userId;
  // editUserDetils: userDetails;

  constructor(private fb: FormBuilder, private newpostService: NewpostService, public keycloak: KeycloakService, private router: Router, private activeRoute: ActivatedRoute, public userService: UserKeyCloakService) {
    this.loggedinUserName = this.userService.getUsername();
    // console.log(this.loggedinUserName);
  }

  ngOnInit() {
    const personlDetails = new userDetails();
    this.createPersonalFormMethod(personlDetails);
    this.getPersonalDetails();

    this.activeRoute.paramMap.subscribe(params => {
      this.userId = +params.get('id');
      if (this.userId) {
        this.getUser(this.userId);
      }
    })
  }

  getUser(id) {
    this.newpostService.getUser(id).subscribe(
      (user: userDetails) => {
        this.myDetails = user[0].filter(e => e.postedby == this.loggedinUserName);
        // console.log('testt' + this.myDetails[0].postedby);
      },
      (err: any) => console.log(err)
    )
  }

  createPersonalFormMethod(mydetails: userDetails) {
    this.createPersonalDetails = this.fb.group({
      tusername: [''],
      tpassword: [''],
      fusername: [''],
      fpassword: [''],
      iusername: [''],
      ipassword: [''],
      whatupsno: [''],
      emailid: [''],
      postedby: ['']
    });
  }

  getPersonalDetails() {
    this.newpostService.getPersonalDetails().subscribe(data => {
      this.personalDetails = data;
      this.myDetails = this.personalDetails.filter(e => e.postedby == this.loggedinUserName);
    })
  }

  saveDetails() {
    if (this.createPersonalDetails.valid) {
      const createPersonaldetailsRequest = new userDetails();

      createPersonaldetailsRequest.tusername = this.createPersonalDetails.get('tusername').value;
      createPersonaldetailsRequest.tpassword = this.createPersonalDetails.get('tpassword').value;
      createPersonaldetailsRequest.fusername = this.createPersonalDetails.get('fusername').value;
      createPersonaldetailsRequest.fpassword = this.createPersonalDetails.get('fpassword').value;
      createPersonaldetailsRequest.iusername = this.createPersonalDetails.get('iusername').value;
      createPersonaldetailsRequest.ipassword = this.createPersonalDetails.get('ipassword').value;
      createPersonaldetailsRequest.whatupsno = this.createPersonalDetails.get('whatupsno').value;
      createPersonaldetailsRequest.emailid = this.createPersonalDetails.get('emailid').value;
      createPersonaldetailsRequest.postedby = this.createPersonalDetails.get('postedby').value;

      this.newpostService.saveDetails(createPersonaldetailsRequest).subscribe(
        personalDetails => {
          $('.soc-alert').addClass('alert alert-success');
          setTimeout(() => {
            $('.soc-alert').addClass('soc-alert-hide');
          }, 5000);

          this.loader = false;

          this.statusMessage = 'Successfully Saved.!';
          this.createPersonalDetails.reset();
        },
        err => {
          $('.soc-alert').addClass('alert alert-danger');
          setTimeout(() => {
            $('.soc-alert').addClass('soc-alert-hide');
          }, 5000);
          this.statusMessage = 'Problem with the services.! please try after sometime.!';
        }
      )
    }
  }

  // editMyPersonalData(myData: userDetails) {
  //   this.createPersonalDetails.patchValue({
  //     tusername: myData.tusername
  //   });
  // }

  loaderDisplay() {
    window.scroll(0, 0);
    this.loader = true;
  }

}
