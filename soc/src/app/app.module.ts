import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { KeycloakService } from 'src/app/shared/soc-keycloak/services';
import { initializer } from 'src/app/app.init';
import { AppAuthGuard } from 'src/app/app-auth-guard';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SocKeycloakModule } from './shared/soc-keycloak/soc-keycloak.module';
import { ServicesModule } from './shared/services/services.module';
import { RecentPostComponent } from './recent-post/recent-post.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NewPostComponent } from './new-post/new-post.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AboutCompanyComponent } from './about-company/about-company.component';
import { EditDetailsComponent } from './edit-details/edit-details.component';

@NgModule({
  declarations: [
    AppComponent,
    RecentPostComponent,
    NavbarComponent,
    NewPostComponent,
    PersonalDetailsComponent,
    PrivacyPolicyComponent,
    AboutCompanyComponent,
    EditDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SocKeycloakModule,
    RouterModule.forRoot(ROUTES , { useHash: true }),
    ServicesModule
  ],
  providers: [
    AppAuthGuard,
    KeycloakService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
