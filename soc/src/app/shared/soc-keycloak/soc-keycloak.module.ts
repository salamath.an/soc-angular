import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { KeycloakBearerInterceptor } from './interceptors';
import { KeycloakService } from './services';

/**
 * The Keycloak Angular Module.
 *
 * Provides the KeycloakService and the bearer interceptor.
 */
@NgModule({
  providers: [
    KeycloakService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakBearerInterceptor,
      multi: true
    }
  ]
})
export class SocKeycloakModule {}
