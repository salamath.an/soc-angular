import { ResourceRoles } from './resource-roles';

describe('ResourceRoles', () => {

  let resourceRoles: ResourceRoles;

  it('assigns the values through constructor', () => {

    const constructorValues = {
        name: 'myName',
        roles: ['myRoleOne', 'myRoleTwo']
    };
    resourceRoles = new ResourceRoles(constructorValues);
    expect(resourceRoles.name).toBe('myName');
    expect(resourceRoles.roles.length).toBe(2);
    expect(resourceRoles.roles[0]).toBe('myRoleOne');
    expect(resourceRoles.roles[1]).toBe('myRoleTwo');
  });

});
