export class ResourceRoles {
    name: string;
    roles: string[];

    constructor(values: object = {}) {
        Object.assign(this, values);
    }
}
