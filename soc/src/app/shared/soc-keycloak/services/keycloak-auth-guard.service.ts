import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { ResourceRoles } from '../model';
import { KeycloakService } from './';

/**
 * A simple guard implementation out of the box. This class should be inherited and
 * implemented by the application. The only method that should be implemented is #isAccessAllowed.
 * The reason for this is that the authorization flow is usually not unique, so in this way you will
 * have more freedom to customize your authorization flow.
 */
export abstract class KeycloakAuthGuard implements CanActivate {
  /**
   * Indicates if the user is authenticated or not.
   */
  protected authenticated: boolean;
  /**
   * Resource-Roles of the loggedIn user. It contains roles for all resources the user has access to.
   */
  protected resourceRoles: ResourceRoles[];

  constructor(
    protected router: Router,
    protected keycloakAngular: KeycloakService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      try {
        this.authenticated = await this.keycloakAngular.isLoggedIn();
        this.resourceRoles = this.keycloakAngular.getResourceRoles();
        const result = await this.isAccessAllowed(route, state);
        resolve(result);
      } catch (error) {
        reject('An error happened during access validation. Details:' + error);
      }
    });
  }

  abstract isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean>;
}
