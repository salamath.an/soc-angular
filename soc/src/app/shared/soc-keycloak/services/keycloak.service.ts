import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Keycloak from 'keycloak-js';
import { Observable, Observer, Subject } from 'rxjs';
import { IKeycloakOptions } from '../interfaces';
import { ResourceRoles } from '../model';

/**
 * Service to expose existent methods from the Keycloak JS adapter, adding new
 * functionalities to improve the use of keycloak in Angular v > 4.3 applications.
 *
 * This class should be injected in the application bootstrap, so the same instance will be used
 * along the web application.
 */
@Injectable()
export class KeycloakService {
  public instance: Keycloak.KeycloakInstance;
  private bearerIncludedUrlPatterns: string[];
  private defaultValidity = 10;

  init(options: IKeycloakOptions = {}): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.bearerIncludedUrlPatterns = options.bearerIncludedUrlPatterns || [];
      this.instance = Keycloak(options.config);
      this.instance
        .init(options.initOptions)
        .success(async authenticated => {
          resolve(authenticated);
        })
        .error(error => {
          reject('An error happened during Keycloak initialization.');
        });
    });
  }

  login(options: Keycloak.KeycloakLoginOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getKeycloakInstance()
        .login(options)
        .success(async () => {
          resolve();
        })
        .error(error => {
          reject('An error happened during the login.');
        });
    });
  }

  logout(redirectUri?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const options: any = {
        redirectUri
      };

      this.getKeycloakInstance()
        .logout(options)
        .success(() => {
          resolve();
        })
        .error(error => {
          reject('An error happened during logout.');
        });
    });
  }

  register(
    options: Keycloak.KeycloakLoginOptions = { action: 'register' }
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      this.instance
        .register(options)
        .success(() => {
          resolve();
        })
        .error(() => {
          reject('An error happened during the register execution');
        });
    });
  }

  isUserInRole(role: string, resource?: string): boolean {
    let hasRole: boolean;
    hasRole = resource
      ? this.getKeycloakInstance().hasResourceRole(role, resource)
      : this.getKeycloakInstance().hasResourceRole(role);

    return hasRole;
  }

  getResourceRoles(): ResourceRoles[] {
    let roles: ResourceRoles[] = [];

    if (this.getKeycloakInstance().resourceAccess) {
      for (const key in this.getKeycloakInstance().resourceAccess) {
        if (this.getKeycloakInstance().resourceAccess.hasOwnProperty(key)) {
          const resourceAccess: any = this.getKeycloakInstance().resourceAccess[
            key
          ];
          const clientRoles = resourceAccess.roles || [];

          const resource = {
            name: key,
            roles: clientRoles
          };

          roles = roles.concat(new ResourceRoles(resource));
        }
      }
    }
    return roles;
  }

  isLoggedIn(): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.updateToken(this.defaultValidity);
        resolve(true);
      } catch (error) {
        resolve(false);
      }
    });
  }

  isTokenExpired(minValidity: number = 0): boolean {
    return this.getKeycloakInstance().isTokenExpired(minValidity);
  }

  updateToken(minValidity: number = 5): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (!this.getKeycloakInstance()) {
        reject(false);
        return;
      }

      this.getKeycloakInstance()
        .updateToken(minValidity)
        .success(refreshed => {
          resolve(refreshed);
        })
        .error(error => {
          reject('Failed to refresh the token, or the session is expired');
        });
    });
  }

  getToken(): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.updateToken(this.defaultValidity);
        resolve(this.getKeycloakInstance().token);
      } catch (error) {
        resolve(undefined);
      }
    });
  }

  getTokenParsed(): Promise<object> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.updateToken(this.defaultValidity);
        resolve(this.getKeycloakInstance().tokenParsed);
      } catch (error) {
        this.login();
      }
    });
  }

  getUsername = function() {
    return this.getKeycloakInstance().tokenParsed.preferred_username;
  };

  getEmail = function() {
    return this.getKeycloakInstance().tokenParsed.email;
  };

  getName = function() {
    return this.getKeycloakInstance().tokenParsed.name;
  };

  getGivenName = function() {
    return this.getKeycloakInstance().tokenParsed.given_name;
  };

  getFamilyName = function() {
    return this.getKeycloakInstance().tokenParsed.family_name;
  };

  getCountryCode = function() {
    return this.getKeycloakInstance().tokenParsed.address_country_code;
  };

  getCity = function() {
    return this.getKeycloakInstance().tokenParsed.address_city;
  };

  getStreet = function() {
    return this.getKeycloakInstance().tokenParsed.address_street;
  };

  getAdditionalAddress = function() {
    return this.getKeycloakInstance().tokenParsed.address_addition;
  };

  getPhoneNo = function() {
    return this.getKeycloakInstance().tokenParsed.address_phone;
  };

  getMobileNo = function() {
    return this.getKeycloakInstance().tokenParsed.address_mobile;
  };

  getHouseNo = function() {
    return this.getKeycloakInstance().tokenParsed.address_number;
  };

  getPostalCode = function() {
    return this.getKeycloakInstance().tokenParsed.address_zip;
  };

  getUserDealerIds = function() {
    return this.getKeycloakInstance().tokenParsed.gevis_number;
  };

  clearToken(): void {
    this.getKeycloakInstance().clearToken();
  }

  addTokenToHeader(headersArg?: HttpHeaders): Observable<HttpHeaders> {
    return Observable.create(async (observer: Observer<any>) => {
      let headers = headersArg;
      if (!headers) {
        headers = new HttpHeaders();
      }
      try {
        const token: string = await this.getToken();
        if (token) {
          headers = headers.set('Authorization', 'Bearer ' + token);
        }
      } finally {
        observer.next(headers);
        observer.complete();
      }
    });
  }

  getKeycloakInstance(): Keycloak.KeycloakInstance {
    return this.instance;
  }

  getBearerIncludedUrlPatterns(): string[] {
    return this.bearerIncludedUrlPatterns;
  }
}
