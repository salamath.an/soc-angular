import { inject, TestBed } from '@angular/core/testing';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from '.';
import { ResourceRoles } from '../model';

const mockSnapshot: any = { snapshot: {}, url: '/cookies'};

const mockRouter: any = { snapshot: {}, url: '/cookies'};

// const mockSnapshot: any = jasmine.createSpyObj<RouterStateSnapshot>(
//     'RouterStateSnapshot', '');

// const mockRouter: any = jasmine.createSpyObj<Router>(
//     'navigateByUrl',
//     ['toString']
//   );

describe(`KeycloakAuthGuard`, () => {
  let spy: any;
  class TestKeycloakAuthGuard extends KeycloakAuthGuard {
    isAccessAllowed(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Promise<boolean> {
      return Promise.resolve(true);
    }
  }

  let testAuthGuard: TestKeycloakAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        KeycloakService,
        { provide: RouterStateSnapshot, useValue: mockSnapshot },
        { provide: Router, useValue: mockRouter }
      ]
    });
  });

  it(
    'should get Promise from canActivate',
    inject(
      [Router, KeycloakService],
      async (router: Router, keycloakAngular: KeycloakService) => {
        testAuthGuard = new TestKeycloakAuthGuard(router, keycloakAngular);
        spy = spyOn(keycloakAngular, 'getResourceRoles').and.returnValue({
          roles: []
        });

        let result;
        try {
          result = await testAuthGuard.canActivate(
            new ActivatedRouteSnapshot(),
            mockSnapshot
          );
        } catch (err) {
          expect(err).toBeUndefined();
        }
        expect(result).toBeDefined();
      }
    )
  );

  it(
    'should get reject from canActivate if KeycloakService not available',
    inject([Router], async (router: Router) => {
      testAuthGuard = new TestKeycloakAuthGuard(router, new KeycloakService());

      let result;
      try {
        result = await testAuthGuard.canActivate(
          new ActivatedRouteSnapshot(),
          mockSnapshot
        );
      } catch (err) {
        expect(err).toBeDefined();
      }
      expect(result).toBeUndefined();
    })
  );
});
