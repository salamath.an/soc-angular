// import { HttpHeaders } from '@angular/common/http';
// import { async, inject, TestBed } from '@angular/core/testing';
// import * as Keycloak from 'keycloak-js';
// import { KeycloakService } from 'src/app/shared/soc-keycloak/services';

// class MockKeycloakInstance {
//   clearToken(): void {
//     return;
//   }

//   updateToken(): Promise<boolean> {
//     return Promise.resolve(true);
//   }

//   success(): void {
//     return;
//   }

//   hasResourceRole(role: string, resource: string): boolean {
//     if (resource) {
//       return true;
//     }
//     return false;
//   }

//   logout(): Promise<boolean> {
//     return Promise.resolve(true);
//   }

//   isTokenExpired(): boolean {
//     return true;
//   }
// }

// describe('KeycloakService', () => {
//   let spy: any;

//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       providers: [KeycloakService]
//     });
//   });

//   it(
//     'init should result in error if there is no keycloak.json file and init parameters.',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       let result;
//       try {
//         result = await service.init();
//       } catch (err) {
//         expect(err).toBeDefined();
//       }
//       expect(result).toBeUndefined();
//     })
//   );

//   it(
//     'init should result in error if there is no valid config initialization for keycloak',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       let result;
//       try {
//         result = await service.init({
//           config: {
//             clientId: '',
//             realm: '',
//             url: ''
//           }
//         });
//       } catch (err) {
//         expect(err).toBeDefined();
//       }
//       expect(result).toBeUndefined();
//     })
//   );

//   it(
//     'init should not result in error if there is valid config initialization for keycloak',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       let result;
//       try {
//         result = await service.init({
//           config: {
//             clientId: 'myClient',
//             realm: 'myRealm',
//             url: 'myUrl'
//           }
//         });
//       } catch (err) {
//         expect(err).toBeUndefined();
//       }
//       expect(result).toBeDefined();
//     })
//   );

//   it(
//     'should return email of parsedToken',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       spy = spyOn(service, 'getKeycloakInstance').and.returnValue({
//         tokenParsed: { email: 'myEmail' }
//       });

//       expect(service.getEmail()).toBe('myEmail');
//     })
//   );

//   it(
//     'should return preferred_username of parsedToken',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       spy = spyOn(service, 'getKeycloakInstance').and.returnValue({
//         tokenParsed: { preferred_username: 'myName' }
//       });

//       expect(service.getUsername()).toBe('myName');
//     })
//   );

//   it(
//     'should call KeycloakInstance.clearToken',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       const mockInstance = new MockKeycloakInstance();

//       function getMockInstance(): MockKeycloakInstance {
//         return mockInstance;
//       }

//       spy = spyOn(service, 'getKeycloakInstance').and.callFake(getMockInstance);

//       spyOn(mockInstance, 'clearToken');

//       service.clearToken();

//       expect(mockInstance.clearToken).toHaveBeenCalled();
//     })
//   );

//   it(
//     'should call KeycloakInstance.hasResourceRole with given resource',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       const mockInstance = new MockKeycloakInstance();

//       function getMockInstance(): MockKeycloakInstance {
//         return mockInstance;
//       }

//       spy = spyOn(service, 'getKeycloakInstance').and.callFake(getMockInstance);

//       expect(service.isUserInRole('role', 'resource')).toBe(true);
//     })
//   );

//   it(
//     'should call KeycloakInstance.hasResourceRole without given resource',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       const mockInstance = new MockKeycloakInstance();

//       function getMockInstance(): MockKeycloakInstance {
//         return mockInstance;
//       }

//       spy = spyOn(service, 'getKeycloakInstance').and.callFake(getMockInstance);

//       expect(service.isUserInRole('role')).toBe(false);
//     })
//   );

//   it(
//     'should deliver resourceRoles from KeycloakInstance',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       spy = spyOn(service, 'getKeycloakInstance').and.returnValue({
//         resourceAccess: {
//           TEST: {
//             roles: ['ADMIN', 'READ']
//           }
//         }
//       });

//       expect(service.getResourceRoles()).toBeTruthy();
//     })
//   );

//   it(
//     'login should result in error if there is init parameters.',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       let result;
//       try {
//         result = await service.login();
//       } catch (err) {
//         expect(err).toBeDefined();
//       }
//       expect(result).toBeUndefined();
//     })
//   );

//   it(
//     'login should result in error if there is no valid config initialization for keycloak',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       let result;
//       try {
//         result = await service.login({});
//       } catch (err) {
//         expect(err).toBeDefined();
//       }
//       expect(result).toBeUndefined();
//     })
//   );

//   it(
//     'logout should call instance.logout',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       let result;
//       try {
//         result = await service.logout('/home');
//       } catch (err) {
//         expect(err).toBeDefined();
//       }
//       expect(result).toBeUndefined();
//     })
//   );

//   it(
//     'isTokenExpired should call instance.isTokenExpired',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       const mockInstance = new MockKeycloakInstance();

//       function getMockInstance(): MockKeycloakInstance {
//         return mockInstance;
//       }

//       spy = spyOn(service, 'getKeycloakInstance').and.callFake(getMockInstance);

//       expect(service.isTokenExpired(5)).toBe(true);
//     })
//   );

//   it(
//     'getToken should call instance.token',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       const mockInstance = new MockKeycloakInstance();

//       function getMockInstance(): MockKeycloakInstance {
//         return mockInstance;
//       }

//       spy = spyOn(service, 'getKeycloakInstance').and.callFake(getMockInstance);

//       spyOn(service, 'updateToken').and.returnValue(true);

//       let result;
//       try {
//         result = await service.getToken();
//       } catch (err) {
//         expect(err).toBeDefined();
//       }
//       expect(result).toBeUndefined();
//     })
//   );

//   it(
//     'getTokenParsed should call instance.tokenParsed',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       const mockInstance = new MockKeycloakInstance();

//       function getMockInstance(): MockKeycloakInstance {
//         return mockInstance;
//       }

//       spy = spyOn(service, 'getKeycloakInstance').and.callFake(getMockInstance);

//       spyOn(service, 'updateToken').and.returnValue(true);

//       let result;
//       try {
//         result = await service.getTokenParsed();
//       } catch (err) {
//         expect(err).toBeDefined();
//       }
//       expect(result).toBeUndefined();
//     })
//   );

//   it(
//     'should give bearerIncludedUrlPatterns from options',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       let result;
//       try {
//         result = await service.init({
//           config: {
//             clientId: 'myClient',
//             realm: 'myRealm',
//             url: 'myUrl'
//           },
//           bearerIncludedUrlPatterns: ['home', 'test']
//         });
//       } catch (err) {
//         expect(err).toBeUndefined();
//       }
//       expect(result).toBeDefined();

//       expect(service.getBearerIncludedUrlPatterns().length).toBe(2);
//     })
//   );

//   it(
//     'should add token to header',
//     inject([KeycloakService], async (service: KeycloakService) => {
//       spy = spyOn(service, 'getToken').and.returnValue('myToken');

//       const headers = new HttpHeaders();

//       service.addTokenToHeader(headers).subscribe(value => {
//         expect(value.get('Authorization')).toBe('Bearer myToken');
//       });
//     })
//   );
// });
