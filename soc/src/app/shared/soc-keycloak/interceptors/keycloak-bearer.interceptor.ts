import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { KeycloakService } from '../services';

/**
 * This interceptor includes the bearer by default in all HttpClient requests.
 *
 * If you need to exclude some URLs from adding the bearer, please, take a look
 * at the {@link KeycloakOptions} bearerExcludedUrls property.
 */
@Injectable()
export class KeycloakBearerInterceptor implements HttpInterceptor {
  private includedUrlsRegex: RegExp[];

  /**
   * KeycloakBearerInterceptor constructor.
   *
   * @param keycloak - Injected KeycloakService instance.
   */
  constructor(private keycloak: KeycloakService) {}

  private loadIncludedUrlsRegex() {
    const includedUrlPatterns: string[] = this.keycloak.getBearerIncludedUrlPatterns();
    this.includedUrlsRegex =
      includedUrlPatterns.map(urlPattern => new RegExp(urlPattern, 'i')) || [];
  }

  /**
   * Intercept implementation that checks if the request url matches the includedUrls.
   * If yes, adds the Authorization header to the request.
   *
   */
  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // If keycloak service is not initialized yet
    if (!this.keycloak || !this.keycloak.getBearerIncludedUrlPatterns()) {
      return next.handle(req);
    }

    if (!this.includedUrlsRegex) {
      this.loadIncludedUrlsRegex();
    }

    const urlRequest = req.url;
    const shallUseToken: boolean = !!this.includedUrlsRegex.find(regex =>
      regex.test(urlRequest)
    );
    if (!shallUseToken) {
      return next.handle(req);
    }

    return this.keycloak.addTokenToHeader(req.headers).pipe(
      mergeMap(headersWithBearer => {
        const kcReq = req.clone({ headers: headersWithBearer });
        return next.handle(kcReq);
      })
    );
  }
}
