import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
  HttpHeaders
} from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { async, inject, TestBed } from '@angular/core/testing';
import { Observable, Observer } from 'rxjs';
import { KeycloakService } from '../services';
import { KeycloakBearerInterceptor } from './keycloak-bearer.interceptor';

describe(`AuthHttpInterceptor`, () => {
  class MockKeycloakService {
    getBearerIncludedUrlPatterns() {
      return ['/included'];
    }

    addTokenToHeader(headersArg?: HttpHeaders): Observable<HttpHeaders> {
      return Observable.create(async (observer: Observer<any>) => {
        let headers = headersArg;
        if (!headers) {
          headers = new HttpHeaders();
        }
        try {
          headers = headers.set('Authorization', 'Bearer fake');
          observer.next(headers);
          observer.complete();
        } catch (error) {
          observer.error(error);
        }
      });
    }
  }

  let mockKeycloak: MockKeycloakService;

  beforeEach(() => {
    mockKeycloak = new MockKeycloakService();
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [
        {
          provide: KeycloakService,
          useValue: mockKeycloak
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: KeycloakBearerInterceptor,
          multi: true
        }
      ]
    });
  });

  afterEach(
    inject([HttpTestingController], (httpMock: HttpTestingController) => {
      httpMock.verify();
    })
  );

  it(
    'adds Authorization header',
    inject(
      [HttpClient, HttpTestingController],
      (http: HttpClient, httpMock: HttpTestingController) => {
        http.get('/included').subscribe(response => {
          expect(response).toBeTruthy();
        });

        const req = httpMock.expectOne('/included');

        expect(req.request.headers.get('Authorization')).toBe('Bearer fake');

        expect(req.request.method).toEqual('GET');

        req.flush({ hello: 'world' });
        httpMock.verify();
      }
    )
  );

  it(
    'adds no Authorization header if url is not included',
    inject(
      [HttpClient, HttpTestingController],
      (http: HttpClient, httpMock: HttpTestingController) => {
        http.get('/excluded').subscribe(response => {
          expect(response).toBeTruthy();
        });

        const req = httpMock.expectOne('/excluded');

        expect(req.request.headers.has('Authorization')).toBe(false);

        expect(req.request.method).toEqual('GET');

        req.flush({ hello: 'world' });
        httpMock.verify();
      }
    )
  );
});
