import { NgModule } from '@angular/core';
import { UserKeyCloakService } from './userKeyCloakService/userKeyCloakService.service';
import { NewpostService } from './newpost/newpost.service';

@NgModule({
  providers: [
    UserKeyCloakService,
    NewpostService
  ]
})
export class ServicesModule {
}
