import { HttpClient, HttpParameterCodec } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from 'src/app/shared/soc-keycloak/services';
import { UserKeyCloak } from 'src/app/shared/model/userKeyCloakModel/userKeyCloak.model';

@Injectable()
export class UserKeyCloakService {
  private user: UserKeyCloak;

  constructor(public keycloak: KeycloakService, private http: HttpClient) {}

  // getUserRole(): string {
  //   const resourceRoles = this.keycloak.getResourceRoles();
  //   let userRoles: string[] = [];
  //   if (resourceRoles) {
  //     const rolesByNameGEO = resourceRoles.filter(
  //       resourceRole => resourceRole.name === 'uam-ui'
  //     );
  //     userRoles = rolesByNameGEO[0].roles;
  //   }
  //   return userRoles[0];
  // }

  getEmail(): string {
    try {
      return this.keycloak.getEmail();
    } catch (e) {
      return undefined;
    }
  }

  getUsername(): string {
    try {
      return this.keycloak.getUsername();
    } catch (e) {
      return undefined;
    }
  }

  logout(): void {
    const currentUrl = window.location.origin;
    this.keycloak.logout(currentUrl);
  }
}
