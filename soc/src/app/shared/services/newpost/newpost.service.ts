import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Newpost } from '../../model/newpost/newpost.model';
import { map } from "rxjs/operators";
import { userDetails } from '../../model/personaldetails/personal-details.model';

@Injectable({
  providedIn: 'root'
})
export class NewpostService {

  // private URL = "http://3.14.13.152:3000";
  private URL = "http://soctech.in:3000";
  // private URL = "http://localhost:3000";

  constructor(private http: HttpClient) { }

  public getRecentPost() { 
    return this.http.get<Newpost[]>(this.URL);
  }

  public saveNewPost(newPostData: Newpost) {
    const url = this.URL + '/addpost';
    return this.http.post<Newpost>(url, newPostData);
  }

  public saveDetails(newPersonalDetails: userDetails) {
    const url = this.URL + '/adddetails';
    return this.http.post<Newpost>(url, newPersonalDetails);
  }

  public getPersonalDetails() {
    const url = this.URL + '/getPersonalDetails'
    return this.http.get<userDetails[]>(url);
  }

  public getUser(id) {
    const url = this.URL + '/getPersonalDetails' + '/' + id;
    return this.http.get<userDetails>(url);
  }

  public updateUser(id, updatePostData) {
    const url = this.URL + '/adddetails' + '/edit/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<userDetails>(url, updatePostData, httpOptions);
  }

}
