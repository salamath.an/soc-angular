export class UserKeyCloak {
  userName: string;
  email: string;
  organization: string;
}
