import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NewpostService } from '../shared/services/newpost/newpost.service';
import { Newpost } from '../shared/model/newpost/newpost.model';
import { KeycloakService } from '../shared/soc-keycloak/services';
import { UserKeyCloakService } from '../shared/services/userKeyCloakService/userKeyCloakService.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {

  createNewPost: FormGroup;
  public loggedinUserName;
  statusMessage = '';
  loader: boolean;

  constructor(private fb: FormBuilder, private newpostService: NewpostService, public keycloak: KeycloakService, private router: Router, public userService: UserKeyCloakService) {
    this.loggedinUserName = this.userService.getUsername();
    console.log(this.loggedinUserName);
  }

  ngOnInit() {
    this.createNewPost = this.fb.group({
      postdata: ['', Validators.required],
      postedby: ['']
    });
  }

  savePost() {
    if (this.createNewPost.valid) {
      const createNewPostRequest = new Newpost();

      createNewPostRequest.postdata = this.createNewPost.get('postdata').value;
      createNewPostRequest.postedby = this.createNewPost.get('postedby').value;

      console.log(createNewPostRequest.postedby);

      this.newpostService.saveNewPost(createNewPostRequest).subscribe(
        newPost => {
          // alert('Newpost successfully posted.!');
          $('.soc-alert').addClass('alert alert-success');
          setTimeout(() => {
            $('.soc-alert').addClass('soc-alert-hide');
          }, 5000);

          this.loader = false;

          this.statusMessage = 'Newpost successfully posted.!';
          this.createNewPost.reset();
          this.router.navigate([
            '/recent-post'
          ]);
        },
        err => {
          // alert('Faild.!');
          $('.soc-alert').addClass('alert alert-danger');
          setTimeout(() => {
            $('.soc-alert').addClass('soc-alert-hide');
          }, 5000);

          this.statusMessage = 'Problem with the services.! please try after sometime.!';
        }
      )
    }
  }

  loaderDisplay() {
    window.scroll(0,0);
    this.loader = true;
  }

}
