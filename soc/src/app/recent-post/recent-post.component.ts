import { Component, OnInit } from '@angular/core';
import { NewpostService } from '../shared/services/newpost/newpost.service';
import { Newpost } from '../shared/model/newpost/newpost.model';
declare var $: any;

@Component({
  selector: 'app-recent-post',
  templateUrl: './recent-post.component.html',
  styleUrls: ['./recent-post.component.scss']
})
export class RecentPostComponent implements OnInit {

  recentPosrData: Newpost[];
  statusMessage = 'Loading data. Please wait.!';
  loadingSpinner = true;

  startPage: Number;
  paginationLimit: Number;

  constructor(private newpostService: NewpostService) { }

  ngOnInit() {
    this.getRecentPost();
    this.startPage = 0;
    this.paginationLimit = 5;
  }

  getRecentPost() {
    this.newpostService.getRecentPost().subscribe(
      data => {
        this.recentPosrData = data;
        // console.log('Length : ' + this.recentPosrData.length);
      },

      err => {
        $('.soc-alert').removeClass('alert-info');
        $('.soc-alert').addClass('alert-danger');
        this.statusMessage = "Problem with the services.! please try after sometime."
        this.loadingSpinner = false;
      });
  }

  showMoreItems() {
    this.paginationLimit = Number(this.paginationLimit) + 3;
  }
  showLessItems() {
    this.paginationLimit = Number(this.paginationLimit) - 3;
  }

}
