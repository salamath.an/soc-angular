import { Component, OnInit } from '@angular/core';
import { KeycloakService } from '../shared/soc-keycloak/services';
import { UserKeyCloakService } from '../shared/services/userKeyCloakService/userKeyCloakService.service';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  public loggedinUserName = { name: '' };

  constructor(public keycloak: KeycloakService, public userService: UserKeyCloakService) {
    this.loggedinUserName = { name: this.userService.getUsername()};
   }

  ngOnInit() {
  }

  onSignOutClicked() {
    $('.logout-btn').addClass('highlight-color');
    const redirctUrl = window.location.origin;
    this.keycloak.logout(redirctUrl);
  }

}
