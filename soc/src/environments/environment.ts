
// *****************************************************************************************
//   This is a sample SG Connect Client ID for use in testing our whiteapp.
//   You should get your client ID in order to authenticated your application users.

//   https://api.fr.world.socgen/catalog/board?tabs=clients

//   DON'T USE IT FOR PRODUCTION
// ********************************************************************************************** -->

export const environment = {
  production: false,
  settings: {
    keycloakUrl: "http://auth.soctech.in:8080/auth",
    keycloakRealm: "soc",
    keycloakClientId: "soc",
    keycloakBearerIncludedUrlPatterns: [""]
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
