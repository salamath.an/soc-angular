import { environment as defaultEnvironment } from './environment';

export const environmentLoader = new Promise<any>((resolve, reject) => {
  const xmlhttp = new XMLHttpRequest();
  const HTTP_STATUS_SUCCESS = 200;
  xmlhttp.open('GET', './assets/environments/environment.json', true);

  xmlhttp.onload = () => {
    if (xmlhttp.status === HTTP_STATUS_SUCCESS) {
      resolve(JSON.parse(xmlhttp.responseText));
    } else {
      resolve(defaultEnvironment);
    }
  };

  xmlhttp.send();
});
