export const environment = {
  production: true,
  settings: {
    keycloakUrl: "http://auth.soctech.in:8080/auth",
    keycloakRealm: "soc",
    keycloakClientId: "soc",
    keycloakBearerIncludedUrlPatterns: [""]
  }
};
