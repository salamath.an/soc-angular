
/*
    Import all the dependancies
*/

var express = require('express');
var http = require('http');
var mysql = require('mysql');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('myTotalySecretKey');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://soctech.in");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Accept");
    res.setHeader(
        "Access-Control-Allow-Credentials",
        "true"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, DELETE, OPTIONS"
    );
  

    next();
});

var requireJsonContent = () => {
  return (req, res, next) => {
	if (req.headers.referer == undefined) {
        res.status(400).send('Sorry! You do not have permission. Please contact admin' )
    } else {
      next()
   }
  }
}

var corsOptions = {
  origin: 'http://soctech.in',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));


// app.use(cors());

/**
 * Database connection
 */

const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'soc'
});

app.get('/', requireJsonContent(), function (req, res) {
    /**
     * To get a json Response
     */
    con.query("SELECT * FROM new_post ORDER BY id DESC", function (err, result) {
        res.header("Access-Control-Allow-Origin", "http://soctech.in");
	res.header("Access-Control-Allow-Credentials", "true");
        console.log(result);
        res.json(result);
    });
});

app.get('/getPersonalDetails', requireJsonContent(), function (req, res) {
    /**
     * To get a json Response
     */
    con.query("SELECT * FROM personal_details", function(err, result) {
    res.header("Access-Control-Allow-Origin", "http://soctech.in");
	console.log(result);
        res.json(result);
    });
});

app.get('/getPersonalDetails/:id', requireJsonContent(), function (req, res) {
    /**
     * To get a json Response
     */
    con.query("SELECT * FROM personal_details WHERE id = '" + req.params.id + "'", function (err, result) {
        res.header("Access-Control-Allow-Origin", "http://soctech.in");
        console.log('testttt' + result);
        res.json(result);
    });
});

/**
 * Post method to data and pre-populate to the firm
 */
app.post('/addpost', requireJsonContent(), function (req, res) {
    /**
     * Get the recorn based on ID
     */

    var query = "INSERT INTO `new_post` (postdata, postedby) VALUES (";
    query += " '" + req.body.postdata + "',";
    query += " '" + req.body.postedby + "')";

    con.query(query, function (err, result) {
        if (!err) {
            res.json(result);
            console.log('Success.!');
        } else {
            console.log('Failed.!');
        }
    });
});

app.post('/adddetails', requireJsonContent(), function (req, res) {
    /**
     * Get the recorn based on ID
     */
    // var test = aes_encrypt('+req.body.tpassword +', ('A0' + tusername + 'Z9'));

    var encTpassword = "aes_encrypt('"+req.body.tpassword+"', '01"+req.body.postedby+"19')";
    var encfpassword = "aes_encrypt('"+req.body.fpassword+"', '01"+req.body.postedby+"19')";
    var encipassword = "aes_encrypt('"+req.body.ipassword+"', '01"+req.body.postedby+"19')";

    var query = "INSERT INTO `personal_details` (tusername, tpassword, fusername, fpassword, iusername, ipassword, whatupsno, emailid, postedby) VALUES (";
    query += " '" + req.body.tusername + "',";
    query +=  encTpassword + ",";
    query += " '" + req.body.fusername + "',";
    query +=  encfpassword + ",";
    query += " '" + req.body.iusername + "',";
    query +=  encipassword + ",";
    query += " '" + req.body.whatupsno + "',";
    query += " '" + req.body.emailid + "',";
    query += " '" + req.body.postedby + "')";

    //console.log(query+ ' << query:' );
    con.query(query, function (err, result) {
        if (!err) {
            res.json(result);
            console.log('Success.!');
        } else {
            console.log('Failed.!');
        }
    });
});

app.post('/adddetails/edit/:id', requireJsonContent(), function (req, res) {
    var encTpassword = "aes_encrypt('"+req.body.tpassword+"', '01"+req.body.postedby+"19')"; 
    var encfpassword = "aes_encrypt('"+req.body.fpassword+"', '01"+req.body.postedby+"19')";
    var encipassword = "aes_encrypt('"+req.body.ipassword+"', '01"+req.body.postedby+"19')";

    var query = "UPDATE `personal_details` SET";
    query += " `tusername` = '" + req.body.tusername + "', ";
    query += " `tpassword` = " + encTpassword + ", ";
    query += " `fusername` = '" + req.body.fusername + "', ";
    query += " `fpassword` = " + encfpassword + ", ";
    query += " `iusername` = '" + req.body.iusername + "', ";
    query += " `ipassword` = " + encipassword + ", ";
    query += " `whatupsno` = '" + req.body.whatupsno + "', ";
    query += " `emailid` = '" + req.body.emailid + "', ";
    query += " `postedby` = '" + req.body.postedby + "'";

    query += "WHERE `personal_details`.`id` = " + req.params.id + "";

    console.log(query+ ' idddd :' + req.params.id);

    con.query(query, function (err, result) {
        if (err) throw err;
        res.json(result);
    });
});

var server = app.listen('3000', () => {
    console.log('Server started in port 3000');
});
