/*
    Import all the dependancies
*/

var express = require('express');
var http = require('http');
var mysql = require('mysql');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
var Cryptr = require('cryptr');
var cryptr = new Cryptr('myTotalySecretKey');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Accept");
    res.setHeader(
        "Access-Control-Allow-Credentials",
        "true"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, DELETE, OPTIONS"
    );

    next();
});

var originsWhitelist = [
    'http://localhost:4200',      //this is my front-end url for development
    'http://localhost:3000'
];

var corsOptions = {
    origin: function (origin, callback) {
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
    },
    credentials: true
}

app.use(cors(corsOptions));
// app.use(cors());

/**
 * Database connection
 */

const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'soc'
});

app.get('/', function (req, res) {
    /**
     * To get a json Response
     */
    con.query("SELECT * FROM new_post ORDER BY id DESC", function (err, result) {
        res.header("Access-Control-Allow-Origin", "*");
        console.log(result);
        res.json(result);
    });
});

app.get('/getPersonalDetails', function (req, res) {
    /**
     * To get a json Response
     */
    con.query("SELECT * FROM personal_details", function(err, result) {
        res.header("Access-Control-Allow-Origin", "*");
        console.log(result);
        res.json(result);
    });
});

app.get('/getPersonalDetails/:id', function (req, res) {
    /**
     * To get a json Response
     */
    con.query("SELECT * FROM personal_details WHERE id = '" + req.params.id + "'", function (err, result) {
        res.header("Access-Control-Allow-Origin", "*");
        console.log('testttt' + result);
        res.json(result);
    });
});

/**
 * Post method to data and pre-populate to the firm
 */
app.post('/addpost', function (req, res) {
    /**
     * Get the recorn based on ID
     */

    var query = "INSERT INTO `new_post` (postdata, postedby) VALUES (";
    query += " '" + req.body.postdata + "',";
    query += " '" + req.body.postedby + "')";

    con.query(query, function (err, result) {
        if (!err) {
            res.json(result);
            console.log('Success.!');
        } else {
            console.log('Failed.!');
        }
    });
});

app.post('/adddetails', function (req, res) {
    /**
     * Get the recorn based on ID
     */
    // var test = aes_encrypt('+req.body.tpassword +', ('A0' + tusername + 'Z9'));

    var encTpassword = cryptr.encrypt(req.body.tpassword, ('A0'+req.body.postedby+'Z9'));
    var encfpassword = cryptr.encrypt(req.body.fpassword, ('A0'+req.body.postedby+'Z9'));
    var encipassword = cryptr.encrypt(req.body.ipassword, ('A0'+req.body.postedby+'Z9'));

    var query = "INSERT INTO `personal_details` (tusername, tpassword, fusername, fpassword, iusername, ipassword, whatupsno, emailid, postedby) VALUES (";
    query += " '" + req.body.tusername + "',";
    query += " '" + encTpassword + "',";
    query += " '" + req.body.fusername + "',";
    query += " '" + encfpassword + "',";
    query += " '" + req.body.iusername + "',";
    query += " '" + encipassword + "',";
    query += " '" + req.body.whatupsno + "',";
    query += " '" + req.body.emailid + "',";
    query += " '" + req.body.postedby + "')";

    con.query(query, function (err, result) {
        if (!err) {
            res.json(result);
            console.log('Success.!');
        } else {
            console.log('Failed.!');
        }
    });
});

app.post('/adddetails/edit/:id', function (req, res) {
    var encTpassword = cryptr.encrypt(req.body.tpassword, ('A0'+req.body.postedby+'Z9'));
    var encfpassword = cryptr.encrypt(req.body.fpassword, ('A0'+req.body.postedby+'Z9'));
    var encipassword = cryptr.encrypt(req.body.ipassword, ('A0'+req.body.postedby+'Z9'));

    var query = "UPDATE `personal_details` SET";
    query += " `tusername` = '" + req.body.tusername + "', ";
    query += " `tpassword` = '" + encTpassword + "', ";
    query += " `fusername` = '" + req.body.fusername + "', ";
    query += " `fpassword` = '" + encfpassword + "', ";
    query += " `iusername` = '" + req.body.iusername + "', ";
    query += " `ipassword` = '" + encipassword + "', ";
    query += " `whatupsno` = '" + req.body.whatupsno + "', ";
    query += " `emailid` = '" + req.body.emailid + "', ";
    query += " `postedby` = '" + req.body.postedby + "'";

    query += "WHERE `personal_details`.`id` = " + req.params.id + "";

    console.log('idddd :' + req.params.id);

    con.query(query, function (err, result) {
        if (err) throw err;
        res.json(result);
    });
});

var server = app.listen('3000', () => {
    console.log('Server started in port 3000');
});
